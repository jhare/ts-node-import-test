export default class Customer {

  name: String;

  constructor(initialName) {
    this.name = initialName;
  }

  sayName() {
    console.log(`My name is ${this.name}`);
  }
}
